<?php
if(file_exists('config.php')) {
  // Get config data
  include_once('config.php');
  // Load code
  include_once('functions.php');
  include_once('render.php');

  print tamtoggl_render_display($config);

}
else {
  print '<p>config.php is missing and needs to created.</p>';
}