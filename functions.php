<?php

/**
 * Use current time to produce an array of three months of history
 * @return array of dates with with start and end values
 */
function tamtoggl_get_month_list() {
  $date = getdate();
  $dates = array();
  for ($i = 0; $i < 6; $i++) {
    $dates[] = array(
      'year' => $date['year'],
      'mon' => $date['mon'],
      'month' => date('F', strtotime('1-'.$date['mon'].'-'.$date['year'])),
    );
    if($date['mon'] == 1) {
      $date['year'] -= 1;
      $date['mon'] = 12;
    }
    else {
      $date['mon'] -= 1;
    }
    // Add the start and end dates for the month
    $dates[$i] = array_merge(
      $dates[$i],
      tamtoggl_get_date_range(
        $dates[$i]['year'],
        $dates[$i]['mon']
      )
    );
    // Add the cache file name
    $dates[$i]['filename'] = 'toggl-'.$dates[$i]['year'].'-'.$dates[$i]['mon'].'.json';
  }

  return $dates;
}

/**
 * Produce the starting and ending dates for a given month/year
 * @param $year
 * @param $month
 */
function tamtoggl_get_date_range($year, $month) {
  $days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
  $range = array(
    'start' => $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-01',
    'end' => $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-'.$days_in_month,
  );
  return $range;
}

function tamtoggl_get_toggl_data($config, $date) {
  $now = time();
  $today = getdate($now);
  $cached = 'Fetched '.date('y-m-d', $now);

  if(file_exists($date['filename'])) {
    $results = json_decode(file_get_contents($date['filename']));
    // Only expire cache for current month
    if($today['mon'] != $date['mon']) {
       $cached = 'Archived';
    }
    else {
      // Is the cached data older than the interval
      if($now - $config['cache_interval'] > $results->timestamp) {
        unset($results);
        $cached = FALSE;
      }
      else {
        $cached = $results->timestamp - ($now - $config['cache_interval']);
      }
    }
  }
  // No cached data? Fetch it
  if(!isset($results)) {
    $report_url = 'https://toggl.com/reports/api/v2/summary'.
      '?user_agent='.$config['user_agent'].
      '&since='.$date['start'].
      '&until='.$date['end'].
      '&billable=true'.
      '&client_ids='.implode('%2C', $config['clients']
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_USERPWD, $config['api_token'] . ':api_token');
    curl_setopt($ch, CURLOPT_URL, $report_url . '&workspace_id=' . $config['workspace']);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $result = curl_exec($ch);
    if($errno = curl_errno($ch)) {
      $error_message = curl_strerror($errno);
      return array(
        'error' => "cURL error ({$errno}):\n {$error_message}"
      );
    }
    else {
      // Add a timestamp of when the data was fetched
      $results = json_decode($result);
      // TODO: Add massaging function here instead
      $results->timestamp = $now;
      $results->report_date = (object) $date;
      file_put_contents($date['filename'], json_encode($results));
    }
    // Close the handle
    curl_close($ch);
  }
  // Add cache info
  $results->cached = $cached;

  return $results;
}

function tamtoggl_toggl_data_massage($data) {
  // Add timestamp of when the data was fetched for caching purposes
  // Add date data for the period fetched, and to allow stale cache persistance

}

function tamtoggl_process_toggl_data($config, $results) {
  $data = array();
  // Add all clients to list
  if(is_array($config['client_data']) AND count($config['client_data']) > 0) {
    foreach($config['client_data'] AS $client_name => $client_data) {
      $data['totals'][$client_name]['Monthly'] = $config['client_data'][$client_name]['monthly'];
    }
  }
  // Proccess that shit
  if (is_object($results) AND is_array($results->data)) {
    foreach($results->data AS $item) {
      $data['totals'][$item->title->client][$item->title->project] = $item->time;
    }
    // Total the totals
    foreach($data['totals'] AS $client => $projects) {
      if(isset($config['client_data'][$client])) {
        if($config['client_data'][$client]['monthly'] > 0) {
          $hours_used = round(array_sum($projects)/1000/60/60, 1);
          $hours_per_month = $config['client_data'][$client]['monthly'];
          $data['totals'][$client]['Total'] = $hours_used;
          if($hours_used > $hours_per_month) {
            $current_state = '+'.($hours_used - $hours_per_month);
          }
          elseif($hours_used < $hours_per_month) {
            $current_state = '-'.($hours_per_month - $hours_used);
          }
          else {
            $current_state = '~';
          }
          $data['totals'][$client]['State'] = $current_state;
          $data['concise'][] = str_pad($hours_per_month, 5, ' ', STR_PAD_LEFT) .
            ' '.str_pad($hours_used, 6  , ' ', STR_PAD_LEFT).' '.
            ' '.str_pad($current_state, 6, ' ', STR_PAD_LEFT).' ['.$client.'] ';
        }
      }
      else {
        $data['totals'][$client] = $data['concise'][] = str_pad(0, 5, ' ', STR_PAD_LEFT) . ' '.str_pad(0, 6, ' ', STR_PAD_LEFT).' ['.$client.'] needs entry in config.php';
      }
    }

    // Produce commands for monthly reports
    if(isset($results->report_date)) {
      // Produce report commands for each client for the month
      $date = $results->report_date;
      $data['reports']['aht'] = array();
      foreach($config['client_data'] AS $client => $client_data) {
        if(isset($client_data['env'])) {
          foreach($client_data['env'] AS $env => $env_data) {
            $data['reports']['aht-report'][] = 'aht '.$env.' --report --start="'.$date->start.':08:00" --end="'.$date->end.':08:00"';
            if(isset($env_data['audit']) AND count($env_data['audit']) > 0) {
              foreach($env_data['audit'] AS $url) {
                $data['reports']['aht-audit'][] = 'aht '.$env.' audit --report --uri="'.$url.'" |sed -e \'s/'.$config['bastion'].'/'.$config['name'].'/g\' > ./'.$url.'-audit.html && open ./'.$url.'-audit.html';
              }
            }
          }
        }
      }
    }

    return $data;
  }
  return array();
}