# README #

To get started, read the comments in the config.sample.php file and create your own config.php with your client list.

### What is this repository for? ###

* Provide an at-a-glance overview of client hours burned, remaining
* Version 0.1

### How do I get set up? ###

* Get data from Toggle for Client ID's
* Create config.php based on sample provided
* Caching is currently file based, no databases were harmed by this code
* TODO: Debugging flag can be enabled to view raw output

### Contribution guidelines ###

* Keep it simple

### TODO ###
* Adding in a second call to filter your hours from the totals
* Help ensure that date code math works for all timezones
* Make it look pretty

### Who do I talk to? ###

* codexmas (gord.christmas@acquia.com)