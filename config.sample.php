<?php
$config = array(
  'workspace' => 249768,
  'user_agent' => 'yourname_toggl', // Just make this unique, like codexmas_toggl. They use it track API calls
  'api_token' => 'yourtoken',
  'cache_interval' => 60 * 60 * 1,
  'name' => 'Your Name',
  'bastion' => 'yname', // Your bastion username
);

// Client list, obtain Client id's by creating a summary report in Toggl
// In the URL will be these values separate by '%2C'
$config['clients'] = array(
  123456,
);

// Client data
// The Client name must match exactly to the data coming in from the JSON results
// Monthly hour allotment, total hours for contract, not per TAM
// audit line can be omitted if client is remotely hosted
$config['client_data'] = array(
  'Exact Client Name' => array(
    'monthly' => 10,
    'env' => array(
      '@site.env' => array(
        'audit' => array('www.domain.com'),
      ),
    ),
  ),
);