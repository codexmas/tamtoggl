<?php

function tamtoggl_render_display($config) {
  $output = '';

  // Has a period other than the default been selected?
  if(isset($_GET['period'])) {
    $period = (int) $_GET['period'];
  }
  else {
    $period = 0;
  }
  $dates = tamtoggl_get_month_list();
  $output .= tamtoggl_render_month_list($dates, $period);

  // Clear selected cache file
  if(isset($_GET['clear']) AND $_GET['clear'] == $period) {
    if(file_exists($dates[$period]['filename'])){
      unlink($dates[$period]['filename']);
    }
  }

  $data = tamtoggl_get_toggl_data($config, $dates[$period]);
  $cache_status = $data->cached ? $data->cached : 'Now';
  $processed = tamtoggl_process_toggl_data($config, $data);
  // TODO: Add real formatting in tamtoggl_render_month_data
  $output .= '<pre>Month   Used     +/- Account';
  $output .= "\r\n----------------------------------------------------------------\r\n";
  $output .= implode("\r\n", $processed['concise']);
  $output .= "\r\n----------------------------------------------------------------\r\n";
  $output .= implode("\r\n", $processed['reports']['aht-report']);
  $output .= "\r\n----------------------------------------------------------------\r\n";
  $output .= implode("\r\n", $processed['reports']['aht-audit']);
  $output .= '</pre>';
  $output .= '<p>Cache Status: <a href="index.php?period='.$period.'&clear='.$period.'" title="Clear cache">'.$cache_status.'</a></p>';
  return $output;
}

function tamtoggl_render_month_list($dates, $current_period) {
  $output = '';
  foreach($dates AS $period => $date) {
    $date_text = $date['month'].'-'.$date['year'];
    if($current_period == $period) {
      $date_text = '<strong>'.$date_text.'</strong>';
    }
    if($period == 0) {
      $output .= '<a href="index.php">'.$date_text.'</a> ';
    }
    else {
      $output .= '<a href="index.php?period='.$period.'">'.$date_text.'</a> ';
    }
  }
  return $output;
}

function tamtoggl_render_month_data($config, $data) {


}



function debug($data) {
  print '<pre>';
  print_r($data);
  print '</pre>';
}